"""
Get the container definitions stored in container-definitons.json, but updated with all
variables in variables.json.
"""
import sys
import os
import json


if __name__ == "__main__":
	cd = os.path.abspath(os.path.dirname(__file__))

	# read the files
	with open(os.path.join(cd, "container-definitions.json")) as f:
		container_definitions_str = f.read()
	with open(os.path.join(cd, "variables.json")) as f:
		variables = json.load(f)

	# replace each search term in the variables file with its corresponding value
	for variable_key, variable_value in variables.items():
		search_term = '<{}>'.format(variable_key)
		if isinstance(variable_value, unicode):
			container_definitions_str = container_definitions_str.replace(search_term, str(variable_value))
		else:
			search_term = '"{}"'.format(search_term)
			container_definitions_str = container_definitions_str.replace(search_term, str(variable_value))

	# return the updated container definition
	print(container_definitions_str)