# Fightris Server

Fightris server is a manual fork of go-docker-core

To run a websocket connection against a running dev container on Windows:

```
wscat -c http://localhost:8081/socket.io/ -n --no-color
```

# go-docker-core

Go Docker Core is intended to be forked in order to quickly create repositories that can deploy Golang-based docker images to AWS in as little time as possible. It's also a strong focus that local testing can be done easily via docker-compose.

To get the server up and running using Docker Compose, run the following command:

```
docker-compose up
```

This will start up the server on port 8080. The port can be changed in the `docker-compose.yml` file.

For development, you can use the dev service, which will automatically restart the go webserver when any watched file is changed:

```
docker-compose up app_development
```

for the changes to take effect. If changes affect the Dockerfile (e.g. the required modules, the location of the
file run in the CMD, you can rebuild the image by running

```
docker-compose build app_development
```

To see the logs of a running container:
```
docker logs --tail 100 -f app_development
```

**Deployment**

Currently deployment is manual. To create a new image repository simply run:

```
 aws ecr create-repository --repository-name go-base-app
```

For each deploy, the docker image needs to be built and pushed using (from the root go-docker-core directory):

```
docker build . -t go-base-app -f deployments/Dockerfile --target production
docker tag go-base-app:latest 364843010988.dkr.ecr.eu-west-1.amazonaws.com/go-base-app:latest
docker push 364843010988.dkr.ecr.eu-west-1.amazonaws.com/go-base-app:latest
```

Once this is done, the service (see below for setting up a service) needs to be restarted via AWS ECS. This will cause downtime, as for cost reasons there
is no longer a load balancer/dynamic port management on the deployed server.


**Setting Up a New Service**

If you've just forked this repo and need to set up manual or automatic deploys, here are the steps to do so.

First, create a log group:
```
aws logs create-log-group --log-group-name go-base-app
```

Next, register the first task definition. The deployments folder contains a container definition template. The following steps will use project-specific variables deployments/variables.json - ensure they are updated before proceeding.

```
JSON_CONTENT=$(python get_container_definitions.py)
aws ecs register-task-definition --family go-base-app --container-definitions "$JSON_CONTENT"
```

Then, to create the service:

```
 aws ecs create-service --service-name go-base-app --task-definition go-base-app:1 --desired-count 1
```


**Troubleshooting**

On Windows, you may have an issue where, despite the docker-compose.yml file forwarding the port, your docker app
is not accessible from host. This appears to be a recent Docker For Windows limitation, which can be worked around by
installing a prior Docker version:

```
choco install docker-for-windows --version 17.09.1.14687
```
