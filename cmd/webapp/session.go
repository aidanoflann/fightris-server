package main

import (
	"fmt"

	websocket "github.com/gorilla/websocket"
	"github.com/rs/xid"
)

// RunSession : given a newly-connected player, run the main game loop until disconnect or game completion.
// One goroutine of this function exists per player
func RunSession(conn *websocket.Conn) {
	// Create the player & register them to a game
	player := &Player{ID: xid.New().String(), conn: conn}
	game := RegisterPlayer(player)

	// close the connection after this handler function completes
	defer conn.Close()

	// if the game is ready to start, broadcast a game start message.
	readyChannel := make(chan string, 1)
	if game.ReadyToPlay() {
		// start a goroutine to trigger the initial spawn
		go func() {
			BroadcastMessageToGame(game.id, "Room is full! Ready to Play in 3 seconds!")
			game.TriggerBlockSpawn()
			readyChannel <- "Go"
		}()
	} else {
		go func() {
			BroadcastMessageToPlayer(player, "Waiting for opponent to join.")
			readyChannel <- "Wait"
		}()
	}

	// start a goroutine to process player requests
	requestChannel := make(chan string, 1)
	go func() {
		for {
			// This is what we'll parse json into. Key must always be string but the value here could be any interface
			request := Request{}
			// ReadJSON is blocking, it awaits messages from the conn.
			err := conn.ReadJSON(&request)
			if err != nil {
				fmt.Printf("ReadJSON error: %s \n", err)
				requestChannel <- "ERROR"
				break
			}
			
			switch request.Type {
			case MoveRequestType:
				fmt.Printf("Got Move event from client.\n")
				game.TriggerBlockMove(player)
			}
			requestChannel <- "OK"
		}
	}()

	// loop forever waiting for messages
	for {
		select {
		case res := <- readyChannel:
			fmt.Printf("Ready routine processed: %s \n", res)
		case res := <- requestChannel:
			fmt.Printf("Request processed: %s \n", res)
			if res == "ERROR" {
				fmt.Printf("Error on client request - probably a disconnect")
				game.Over = true
				return
			}
		}
	}
}
