package main

import (
	websocket "github.com/gorilla/websocket"
)

// Player : 
type Player struct {
	ID string
	conn *websocket.Conn
	gameID string
	down bool
	score int
}