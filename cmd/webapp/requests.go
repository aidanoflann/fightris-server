package main

// RequestType : enum-style typing for events
type RequestType int

const (
	// MoveRequestType : trigger the block to move towards the user who triggerd the event
	MoveRequestType	 RequestType = 0
 )

// Request : General request structure to be parsed into when handling client requests
type Request struct {
	Type RequestType
	BlockID string
}