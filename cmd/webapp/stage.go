package main


import (
	"math"
)

const (
	width = 10
	height = 16
)

// Stage is a way of tracking information stored on the 2D space of the game, for example occupied positions
type Stage struct {
	takenMap [width][height]int8
	spacing float64
}


// CreateStage creates a Stage with default parameters
func CreateStage() *Stage {
	takenMap := [width][height]int8{}
	return &Stage{
		takenMap : takenMap,
		spacing : 50,
	}
}

// ClearRows will check for any full rows and clear them if found. It will return the index of the original rows cleared.
func (s *Stage)ClearRows() []int {
	// first generate the array of rows to clear
	clearedRows := []int{}
	for y := 0; y < height; y++ {
		rowisFull := true
		for x := 0; x < width; x++ {
			if s.takenMap[x][y] == 0 {
				rowisFull = false
				break
			}
		}
		if rowisFull {
			clearedRows = append(clearedRows, y)
		}
	}

	// iterate over any rows that were cleared, and set their occupied state back to 0
	for _, clearedY := range clearedRows {
		for x := 0; x < width; x++ {
			s.takenMap[x][clearedY] = 0
			
			// cause blocks above the deleted row(s) to drop down
			if (clearedY < height / 2) {
				// move all rows below upwards
				for yBelow := clearedY; yBelow < height / 2; yBelow++ {
					s.takenMap[x][yBelow] = s.takenMap[x][yBelow + 1]
				}

			} else {
				// move all rows above downwards
				for yAbove := clearedY; yAbove > height / 2; yAbove-- {
					s.takenMap[x][yAbove] = s.takenMap[x][yAbove - 1]
				}
			}
		}
	}

	return clearedRows
}


// CheckLoss checks if the state of the map is such that one of the players has lost (there are blocks in the middle of the map)
func (s *Stage)CheckLoss() bool {
	for x := 0; x < width; x++ {
		for y := height/2 -1; y <= height/2; y++ {
			if s.takenMap[x][y] > 0 {
				return true
			}
		}
	}
	return false
}

// GetTakenCoords returns a Coordinate array representing all positions on the stage that are occupied
func (s *Stage)GetTakenCoords() []Coordinate {
	takenCoords := []Coordinate{}
	for x := 0; x < width ; x++ {
		for y := 0; y < height ; y++ {
			if s.takenMap[x][y] > 0 {
				takenCoords = append(takenCoords, Coordinate{X: x, Y: y})
			}
		}
	}
	return takenCoords
}

// AddBlock adds the block of given coordinates to the stage and returns the nearest coordinates on the grid
func (s *Stage)AddBlock(block Block, down bool) (float64, float64) {
	blockX := block.Position.X
	blockY := block.Position.Y
	var xDiff float64
	var yDiff float64
	smallestXDiff := float64(10000)
	smallestYDiff := float64(10000)

	var chosenX int
	var chosenY int

	var column [16]int8

	// Step 1: Find where on the grid the block currently is
	for x, thisColumn := range s.takenMap {
		xDiff = math.Abs(blockX - (float64(x) * s.spacing))
		if xDiff < smallestXDiff {
			smallestXDiff = xDiff
			chosenX = x
		}
		column = thisColumn
	}

	for y := range column {
		yDiff = math.Abs(blockY - (float64(y) * s.spacing))
		if yDiff < smallestYDiff {
			smallestYDiff = yDiff
			chosenY = y
		}
	}

	// Step 2: chosenX and chosenY now point to the block's current coordinates.
	// Slide Y until the block lands on another, then choose the previous value
	var finalChosenY int
	var finalChosenX int
	var forwardOne int
	if down {
		forwardOne = +1
	} else {
		forwardOne = -1
	}
	
	finalChosenX = chosenX
	// first, check if the shape of the block means it needs to be nudged in from the left or rightmost edge
	for _, c := range block.SubBlocks {
		for {
			if finalChosenX + c.X >= len(s.takenMap) {
				// needs to be nudged left
				finalChosenX --
			} else if finalChosenX + c.X < 0 {
				// needs to be nudged right
				finalChosenX ++
			} else {
				break
			}
		}
	}

	for slideY := chosenY; slideY < len(column) && slideY > 0 ; slideY += forwardOne {
		finalChosenY = slideY
		for _, c := range block.SubBlocks {
			if slideY + c.Y >= len(s.takenMap[0]) {
				finalChosenY --
				break
			}
			if slideY + c.Y < 0 {
				finalChosenY ++
				break
			}
			if s.takenMap[finalChosenX + c.X][slideY + c.Y] > 0 {
				finalChosenY -= forwardOne
				break
			}
		}
		if finalChosenY != slideY {
			break
		}
	}

	for _, c := range block.SubBlocks {
		s.takenMap[finalChosenX + c.X][finalChosenY + c.Y] = 1
	}

	return float64(finalChosenX) * s.spacing, float64(finalChosenY) * s.spacing
}