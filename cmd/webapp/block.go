package main

import (
	"time"
)


// Block : represents an ingame fightris block
type Block struct {
	Position Point
	Velocity Point
	TimeOfLastPosition time.Time
	SubBlocks []Coordinate
}

// SnapToCurrentPosition : Figure out where the block should be based on its velocity, and move it there, stopping it.
func (b *Block) SnapToCurrentPosition() {
	currentPosition := b.GetCurrentPosition()
	b.Position = currentPosition
	b.Velocity = Point{0,0}
	b.TimeOfLastPosition = time.Now()
} 

// GetCurrentPosition : calculates the position the block will have moved to from its TimeOfLastPosition until now
func (b Block) GetCurrentPosition() Point {
	timeDelta := time.Now().Sub(b.TimeOfLastPosition).Seconds()
	currentX := b.Position.X + b.Velocity.X * timeDelta
	currentY := b.Position.Y + b.Velocity.Y * timeDelta
	return Point{currentX, currentY}
}