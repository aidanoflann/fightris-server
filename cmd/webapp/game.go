package main

import (
	"fmt"
	"time"
	"math/rand"

	"github.com/rs/xid"
)

const (
	playerLimit = 2
)

// all premade blocks go here
var premadeBlockCoords [6][]Coordinate

// all existing games are stored here
var games map[string]*Game

// Game : Stores references to each player participating in a game, as well as other shared data.
type Game struct {
	id string
	// registered players
	players []*Player
	// all existing blocks are stored here
	blocks map[string]*Block
	// ID of the block that is currently moving across the screen
	currentlyMovingBlockID string
	// for blocking action during cooldown periods
	blockMoveAllowed bool
	// for tracking block locations
	stage *Stage
	Over bool
}

// RegisterPlayer : finds a valid game for the player, or creates one, and adds the player to it.
// Returns True if the game is ready to start
func RegisterPlayer(player *Player) *Game {
	// If the games map is nil, we need to create it (first time only).
	if games == nil {
		fmt.Println("Performing first-time initialization of game.")
		games = make(map[string]*Game)
	} 

	// see if a suitable game already exists...
	var game *Game
	for _, v := range(games) {
		if len(v.players) >= playerLimit {
			continue
		}
		game = v
		break
	}

	// ... and if not, create one
	if game == nil {
		// all games are full, create a new one
		game = CreateGame()
		games[game.id] = game
	}
	game.players = append(game.players, player)
	player.gameID = game.id

	// if the player is the first in the session, they are the "down" player, and the other is "up"
	if len(game.players) == 1 {
		player.down = true
	} else {
		player.down = false
	}

	fmt.Printf("Registered player \n")
	fmt.Printf("Number of games: %d \n", len(games))
	fmt.Printf("Number of players in current game: %d \n", len(game.players))
	
	startEvent := StartEvent {Type:StartEventType, PlayerID:player.ID}
	BroadcastToPlayer(player, startEvent)

	return game
}


// CreateGame creates a Game object with default parameters
func CreateGame() *Game {
	game := &Game{
		id : xid.New().String(),
		players : make([]*Player, 0),
		blocks: make(map[string]*Block),
		blockMoveAllowed: true,
		stage: CreateStage(),
		Over: false,
	}
	fmt.Printf("Created new Game: %s \n", game.id)
	return game
}

// TriggerBlockSpawn : wait three seconds then create a new block
func (g *Game)TriggerBlockSpawn() {
	if g.Over {
		return
	}
	g.blockMoveAllowed = false
	time.Sleep(2 * time.Second)
	// trigger a block spawn
	// genreate block ID
	blockID := xid.New().String()

	// if the cache of premade block layouts is not yet initialised, do so
	if premadeBlockCoords[0] == nil {
		InitialisePremadeBlocks()
	}
	rand.Seed(time.Now().Unix())
	n := rand.Int() % len(premadeBlockCoords)
	block := &Block{Point{600, 375}, Point{-180, 0}, time.Now(), premadeBlockCoords[n]}
	event := SpawnEvent{
		blockID,
		int64(block.Position.X),
		int64(block.Position.Y),
		int64(block.Velocity.X),
		int64(block.Velocity.Y),
		block.SubBlocks,
		SpawnEventType,
	}
	g.blocks[blockID] = block
	g.currentlyMovingBlockID = blockID
	BroadcastToGame(g.id, event)
	g.blockMoveAllowed = true
	fmt.Println("Triggering removal of block (if no player interaction).")
	go g.TriggerBlockRemove(blockID)
}


// TriggerBlockRemove will wait a set amount of time then, if the specified block is not yet placed, delete it
func (g *Game)TriggerBlockRemove(blockID string) {
	time.Sleep(5 * time.Second)
	block := g.blocks[blockID]
	if block.Velocity.X == 0 {
		// the block was placed, return
		fmt.Println("Block no longer moving, no need to delete.")
		return
	}
	// the block is still moving, delete it and spawn another
	delete(g.blocks, blockID)
	go g.TriggerBlockSpawn()
}


// InitialisePremadeBlocks generates each block type and stores them in the premadeBlockCoords slice.
func InitialisePremadeBlocks() {
	square := make([]Coordinate, 4)
	square[0] = Coordinate{X: 0, Y: 0}
	square[1] = Coordinate{X: -1, Y: 0}
	square[2] = Coordinate{X: -1, Y: -1}
	square[3] = Coordinate{X: 0, Y: -1}

	line := make([]Coordinate, 4)
	line[0] = Coordinate{X: 0, Y: 2}
	line[1] = Coordinate{X: 0, Y: 1}
	line[2] = Coordinate{X: 0, Y: 0}
	line[3] = Coordinate{X: 0, Y: -1}

	zig := make([]Coordinate, 4)
	zig[0] = Coordinate{X: -1, Y: -1}
	zig[1] = Coordinate{X: 0, Y: -1}
	zig[2] = Coordinate{X: 0, Y: 0}
	zig[3] = Coordinate{X: 1, Y: 0}

	zag := make([]Coordinate, 4)
	zag[0] = Coordinate{X: -1, Y: -1}
	zag[1] = Coordinate{X: -1, Y: 0}
	zag[2] = Coordinate{X: 0, Y: 0}
	zag[3] = Coordinate{X: 0, Y: 1}

	tee := make([]Coordinate, 4)
	tee[0] = Coordinate{X: -1, Y: 0}
	tee[1] = Coordinate{X: 0, Y: 0}
	tee[2] = Coordinate{X: 0, Y: 1}
	tee[3] = Coordinate{X: 1, Y: 0}

	wideline := make([]Coordinate, 5)
	wideline[0] = Coordinate{X: -2, Y: 0}
	wideline[1] = Coordinate{X: -1, Y: 0}
	wideline[2] = Coordinate{X: 0, Y: 0}
	wideline[3] = Coordinate{X: 1, Y: 0}
	wideline[4] = Coordinate{X: 2, Y: 0}

	premadeBlockCoords[0] = square
	premadeBlockCoords[1] = line
	premadeBlockCoords[2] = zig
	premadeBlockCoords[3] = zag
	premadeBlockCoords[4] = tee
	premadeBlockCoords[5] = wideline
}


// TriggerBlockMove : snap the specified block to a position closer to the user that triggered it
func (g *Game)TriggerBlockMove(player *Player) {
	if !g.blockMoveAllowed {
		return
	}
	block, ok := g.blocks[g.currentlyMovingBlockID]
	if !ok {
		fmt.Printf("Cannot find block of ID %s \n", g.currentlyMovingBlockID)
	}
	block.SnapToCurrentPosition()

	// slide the block into place and broadcast the event
	block.Position.X, block.Position.Y = g.stage.AddBlock(*block, player.down)
	event := MoveEvent{g.currentlyMovingBlockID, int64(block.Position.X), int64(block.Position.Y), MoveEventType}
	BroadcastToGame(g.id, event)
	
	// check for completed lines and, if a line deletion happens, broadcast the event.
	clearedRows := g.stage.ClearRows()

	// check for loss
	loss := g.stage.CheckLoss()
	if loss {
		fmt.Printf("Player %s has LOST.\n", player.ID)
		// broadcast the loss
		// get the OTHER player
		playerWonEvent := PlayerWonEvent{Type: PlayerWonEventType}
		for _, thisPlayer := range g.players {
			if thisPlayer.ID != player.ID {
				playerWonEvent.PlayerID = thisPlayer.ID
				break
			}
		}
		BroadcastToGame(g.id, playerWonEvent)
		g.Over = true	
	}

	// check for win
	player.score += len(clearedRows)
	if player.score >= 10 {
		fmt.Printf("Player %s has WON.\n", player.ID)
		playerWonEvent := PlayerWonEvent{Type:PlayerWonEventType, PlayerID: player.ID}
		BroadcastToGame(g.id, playerWonEvent)
		g.Over = true
	}

	// update the client with the current stage status
	syncEvent := StageSyncEvent{
		g.stage.GetTakenCoords(),
		g.GetPlayerScores(),
		StageSyncEventType,
	}
	BroadcastToGame(g.id, syncEvent)

	// trigger the next block spawn
	go g.TriggerBlockSpawn()
}


// GetPlayerScores prepares a map of player IDs to their scores
func (g *Game)GetPlayerScores() map[string]int {
	playerScores := map[string]int{}
	for _, player := range g.players {
		playerScores[player.ID] = player.score
	}
	return playerScores
}


// ReadyToPlay : true if the game is now at its limit of players, false otherwise
func (g *Game)ReadyToPlay() bool {
	return len(g.players) >= playerLimit
}