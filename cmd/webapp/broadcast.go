package main

import (
	"fmt"
)


// BroadcastMessageToGame : Broadcast the given msg to all Players in the specified Session
func BroadcastMessageToGame(gameID string, msg string) {
	dat := map[string]string{"message" : msg}
	BroadcastToGame(gameID, dat)
}


// BroadcastToGame : Takes given generic interface and sends it to all players in the specified Session
func BroadcastToGame(gameID string, i interface{}) {
	game, ok := games[gameID]
	if !ok {
		fmt.Printf("Game %s not found \n", gameID)
	}

	for _, player := range(game.players) {
		BroadcastToPlayer(player, i)
	}
}

// BroadcastMessageToPlayer : Take the string and prepare a jsonable interface to broadcast to the specified player
func BroadcastMessageToPlayer(player *Player, msg string) {
	dat := map[string]string{"message" : msg}
	BroadcastToPlayer(player, dat)
} 

// BroadcastToPlayer : send the given interface along the Player's connection
func BroadcastToPlayer(player *Player, i interface{}) {
	err := player.conn.WriteJSON(i)
	if err != nil {
		fmt.Printf("Error broadcasting to player: %s \n", err)
	}
}