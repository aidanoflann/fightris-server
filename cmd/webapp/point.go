package main

// Point represents an x-y coordinate (can be velocity, location, etc.)
type Point struct {
	X float64
	Y float64
}

// Coordinate represents an x-y coordinate on a grid
type Coordinate struct {
	X int
	Y int
}