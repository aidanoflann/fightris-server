package main

// EventType : enum-style typing for events
type EventType int

const (
	// StartEventType : Starting event, used to send initialisation data to client
	StartEventType    EventType = 0
	// SpawnEventType : trigger a block to spawn on the client
	SpawnEventType    EventType = 1
	// MoveEventType : trigger the block to move towards the user who triggerd the event
	MoveEventType	 EventType = 2
	// StageSyncEventType : update the client on the current stage state
	StageSyncEventType	 EventType = 3
	// PlayerWonEventType : signals to the client which player won
	PlayerWonEventType	 EventType = 4
 )


 // StartEvent : Starting event, used to send initialisation data to client
type StartEvent struct {
	PlayerID string
	Type EventType
}

// SpawnEvent : Event to trigger the spawn of a block
type SpawnEvent struct {
	BlockID string
	X int64
	Y int64
	VelocityX int64
	VelocityY int64
	SubBlockCoords []Coordinate
	Type EventType
}

// MoveEvent : Event to trigger movement of a pre-existing block
type MoveEvent struct {
	BlockID string
	X int64
	Y int64
	Type EventType
}

// StageSyncEvent : Event to synchronise the stage state with the client
type StageSyncEvent struct {
	SubBlockCoords []Coordinate
	PlayerScores map[string]int
	Type EventType
}

// PlayerWonEvent : Sent when a player wins to signal the end of the game
type PlayerWonEvent struct { 
	PlayerID string
	Type EventType
}