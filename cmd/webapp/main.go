package main

import (
	"fmt"
	"log"
	"net/http"

	websocket "github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	// upgrade to a websocket connection
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Connected.")
	RunSession(conn)
}

func main() {
	http.HandleFunc("/", loginHandler)
	log.Printf("Serving at localhost:8080... \n")
	log.Fatal(http.ListenAndServe(":8080", nil))
}